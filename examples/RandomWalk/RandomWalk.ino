/*

# Random Walk via XBee

This sketch is a simple implementation of an XBee network which communicates
via Json-RPC where each node acts both as server and as client. Each node
periodically broadcasts a request to calculate the next step of a
one-dimensional random walk (https://en.wikipedia.org/wiki/Random_walk). Every
node can respond to such a request by adding a random floating-point number
between -1 and 1 to the recieved number.

## Hardware setup for this sketch:

Set up two or more copies of the following:

- Arduino MEGA
- Arduino Wireless SD Shield stacked on top
  - Pins 0 and 1 bent away (USB RX/TX)
  - SELECT SERIAL switch set to MICRO (not USB)
  - Connect XBee Serial to Serial1:
    - Connect Pin 0 to Pin MEGA Pin 19
    - Connect Pin 1 to Pin MEGA Pin 18
  - XBee S2C plugged in
    - Zigbee Firmware
    - both XBee's configured correctly to be in the same network
    - read book ”Building Wireless Sensor Networks” for further information

## Software Setup

- Install the libraries #include'd below
- Flash this sketch to all Arduino MEGAs
- Open Serial Monitors for all and watch the progress

*/

// Tell ArduinoJsonRPC to not use the "jsonrpc":"2.0" member to conserve
// payload space
#define ARDUINOJSONRPC_USE_JSONRPC_MEMBER 0

// uncomment the following two settings to see what's going on under the hood
// #define ARDUINOJSONRPC_DEBUG 1
// #define XBEEJSONRPC_DEBUG 1

#include <ArduinoJson.h>
#include <ArduinoJsonRPC.hpp>
#include <XBee.h>
#include <XBeeJsonRPC.hpp>

// Create an Xbee JsonRPC participant (both a Json-RPC client and server)
XBeeJsonRPC::XBeeJsonRPC<5 // number of method callback slots
                         ,
                         500 // StaticJsonDocument capacity for input
                         ,
                         500 // StaticJsonDocument capacity for output
                         >
  xbee;

// global counter variable
float value = 0;

JSONRPCSERVERMETHOD(stepMethod, id, params, response, extra)
{
  // assume that the parameters are an array and extract the first value of it
  const float givenValue = params[0];
  // answer with an array as result
  JsonArray result = response.createNestedArray("result");
  // get a random value
  // NOTE: random() returns an int. To convert it to float, we need to
  // static_cast it so that the division does not truncate the decimal places
  const float randomValue = static_cast<float>(random(-1000, 1000)) / 1000.0F;
  // add up given and random value
  const float newValue = givenValue + randomValue;
  Serial.print(F("Our Server: Adding the random value "));
  Serial.print(randomValue);
  Serial.print(F(" to the given value "));
  Serial.print(givenValue);
  Serial.print(F(" and answering with new value "));
  Serial.print(newValue);
  Serial.println();
  result.add(newValue); // add sum to result
  // return that we processed the request successfully
  return JsonRPC::CallbackStatus::Ok;
}

JSONRPCCLIENTCALLBACK(stepCallback, response, extra)
{
  const char* errorMessage = response["error"]["message"];
  if (errorMessage) {
    Serial.print("Our Client: Step request didn't work. Other side says: ");
    Serial.println(errorMessage);
    return false;
  }
  // interpret the reponse's member "result" as array, assume that the first
  // element is a float and extract that value
  float respondedValue = response["result"][0];
  Serial.print(F("Our Client: Updating our value "));
  Serial.print(value);
  Serial.print(F(" with new value "));
  Serial.print(respondedValue);
  Serial.print(F(" responded from server"));
  Serial.println();
  // adopt responded value
  value = respondedValue;
  // return that we processed the response successfully
  return true;
}

bool
sendStepRequest(void)
{
  // create a JsonDocument (see https://arduinojson.org/v6/assistant)
  StaticJsonDocument<JSON_ARRAY_SIZE(1) + JSON_OBJECT_SIZE(3)> requestDoc;
  // turn the Jsondocument into a generic JsonVariant
  JsonVariant request = requestDoc.to<JsonVariant>();
  // create a request template
  xbee.createRequest(request, "step");
  // add a "params" array
  JsonArray params = request.createNestedArray("params");
  // add our value to the "params"
  params.add(value);
  Serial.print(F("Our Client: Broadcasting step request with value "));
  Serial.println(value);
  // send the request via XBee
  return xbee.sendRequest(request, ZB_BROADCAST_ADDRESS, stepCallback, 3e3);
}

// the setup() function is run ONCE at startup
void
setup(void)
{
  Serial.begin(9600);      // initialize the USB serial interface
  Serial1.begin(9600);     // initialize the XBee serial interface
  xbee.setSerial(Serial1); // talk to the XBee via Serial1
  randomSeed(A0);          // make random() results really random
  // call stepMethod() if a "step" request comes in
  xbee.registerMethod("step", stepMethod);
}

unsigned long timeLastStepRequest = 0; // the time we last did a step request
size_t stepRequestTimeoutMs = 0;

// the loop() function is run over and over again
void
loop(void)
{
  if (millis() - timeLastStepRequest > stepRequestTimeoutMs) {
    const bool stepRequestWorked = sendStepRequest();
    if (not stepRequestWorked)
      Serial.println(
        F("Our Client: WARNING: Sending step request didn't work!"));
    // do next request between 5 and 15 seconds
    stepRequestTimeoutMs = random(10e3, 20e3);
    timeLastStepRequest = millis();
  }
  // check whether the XBee has pending requests/responses and process them
  xbee.loop();
}
