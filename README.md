# XBeeJsonRPC Arduino Library

XBee Json-RPC library

## Dependencies

You will need the following libraries:

- [ArduinoJson](https://arduinojson.org)
- [ArduinoJsonRPC](https://gitlab.com/tue-umphy/co2mofetten/arduino-libraries/ArduinoJsonRPC)
- [XBee](https://github.com/andrewrapp/xbee-arduino)

## Documentation

For now, see the
[`XBeeJsonRPC.hpp`](https://gitlab.com/tue-umphy/co2mofetten/arduino-libraries/XBeeJsonRPC/blob/master/XBeeJsonRPC.hpp)
file for a method description or have a look at the `examples` folder.



