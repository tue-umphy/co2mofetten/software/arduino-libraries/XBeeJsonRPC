#pragma once

/*

XBee + JsonRPC

*/

#include <Arduino.h>
#include <ArduinoJson.h>
#include <ArduinoJsonRPC.hpp>

#if XBEEJSONRPC_DEBUG
#include <Printers.h>
#endif // #if XBEEJSONRPC_DEBUG

#include <XBee.h>

#if XBEEJSONRPC_DEBUG
#define XBJRPCDBG(cmd)                                                        \
  {                                                                           \
    Serial.print("XBeeJsonRPC: ");                                            \
    cmd;                                                                      \
    Serial.println();                                                         \
    Serial.flush();                                                           \
  };
#else // #if XBEEJSONRPC_DEBUG
#define XBJRPCDBG(cmd)
#endif // #if XBEEJSONRPC_DEBUG

namespace XBeeJsonRPC {

using namespace JsonRPC;

const uint8_t xbeeMaxPayloadSize = 72;

template<size_t N_METHODS, size_t INPUT_CAP, size_t OUTPUT_CAP>
class XBeeJsonRPC
  : public XBeeWithCallbacks
  , public JsonRPC::Participant<N_METHODS>
{
public:
  XBeeJsonRPC(void);
  static void processPacket(ZBRxResponse& rx, uintptr_t);
  bool sendRequest(const JsonVariantConst request,
                   XBeeAddress64 address,
                   RequestCallback_t callback = nullptr,
                   const size_t timeout = 0);
  bool sendNotification(const JsonVariantConst notification,
                        XBeeAddress64 address,
                        const size_t timeout = 0);
  void broadcast(const JsonVariantConst request);

private:
};

template<size_t N_METHODS, size_t INPUT_CAP, size_t OUTPUT_CAP>
XBeeJsonRPC<N_METHODS, INPUT_CAP, OUTPUT_CAP>::XBeeJsonRPC(void)
{
  this->onZBRxResponse(XBeeJsonRPC::processPacket,
                       reinterpret_cast<uintptr_t>(this));

#if XBEEJSONRPC_DEBUG
  // Logging the errors to the Serial port
  this->onPacketError(
    printErrorCb, reinterpret_cast<uintptr_t>(static_cast<Print*>(&Serial)));
  this->onTxStatusResponse(
    printErrorCb, reinterpret_cast<uintptr_t>(static_cast<Print*>(&Serial)));
  this->onZBTxStatusResponse(
    printErrorCb, reinterpret_cast<uintptr_t>(static_cast<Print*>(&Serial)));
#endif // #if XBEEJSONRPC_DEBUG
}

template<size_t N_METHODS, size_t INPUT_CAP, size_t OUTPUT_CAP>
void
XBeeJsonRPC<N_METHODS, INPUT_CAP, OUTPUT_CAP>::processPacket(ZBRxResponse& rx,
                                                             uintptr_t extra)
{
  // It is not possible in C++ to use an instance method as a callback pointer.
  // Luckily, XBeeWithCallbacks has implemented passing an extra argument to
  // the callbacks. We just pass the pointer to the object to make it available
  // in this callback.
  XBeeJsonRPC& THIS = *reinterpret_cast<XBeeJsonRPC*>(extra);
  StaticJsonDocument<INPUT_CAP> inputDoc;
  StaticJsonDocument<OUTPUT_CAP> outputDoc;
  JsonVariant response = outputDoc.template to<JsonVariant>();
  DeserializationError err = deserializeMsgPack(
    inputDoc, reinterpret_cast<const char*>(rx.getData()), rx.getDataLength());
  if (not err) {
    XBJRPCDBG(Serial.print(F("input document usage: "));
              Serial.print(inputDoc.memoryUsage());
              Serial.print("/");
              Serial.print(inputDoc.capacity()););
  }
  if (err == DeserializationError::Ok) {
    XBJRPCDBG(Serial.print(F("XBee: Recieved packet containing MsgPack: "));
              serializeJson(inputDoc, Serial);
              Serial.print(F(" from address 0x"));
              Serial.print(rx.getRemoteAddress64().getMsb(), HEX);
              Serial.print(rx.getRemoteAddress64().getLsb(), HEX););
    JsonVariant input = inputDoc.template as<JsonVariant>();
    ProcessStatus status = THIS.process(input, response, &rx);
    if (status == ProcessStatus::ResponseProcessed or
        status == ProcessStatus::NotificationProcessed or
        status == ProcessStatus::ResponseError or
        status == ProcessStatus::ResponseIgnored)
      return;
  } else {
    XBJRPCDBG(Serial.print(F("XBee: MsgPack parsing error: "));
              Serial.print(err.c_str()););
    THIS.createError(response, ErrorCode::ParseError);
  }
  XBJRPCDBG(Serial.print(F("output document usage: "));
            Serial.print(outputDoc.memoryUsage());
            Serial.print("/");
            Serial.print(outputDoc.capacity());)
  // answer to the request
  ZBTxRequest txRequest;
  txRequest.setAddress64(rx.getRemoteAddress64());
  char msgPackPayload[xbeeMaxPayloadSize];
  const size_t msgPackSize = measureMsgPack(response);
  serializeMsgPack(response, msgPackPayload);
  txRequest.setPayload(reinterpret_cast<uint8_t*>(msgPackPayload),
                       msgPackSize <= xbeeMaxPayloadSize ? msgPackSize
                                                         : xbeeMaxPayloadSize);
  XBJRPCDBG(Serial.print(F("XBee: Sending MsgPack "));
            serializeJson(response, Serial);
            Serial.print(F(" to address 0x"));
            Serial.print(txRequest.getAddress64().getMsb(), HEX);
            Serial.print(txRequest.getAddress64().getLsb(), HEX););
  THIS.send(txRequest);
}

template<size_t N_METHODS, size_t INPUT_CAP, size_t OUTPUT_CAP>
bool
XBeeJsonRPC<N_METHODS, INPUT_CAP, OUTPUT_CAP>::sendRequest(
  const JsonVariantConst request,
  XBeeAddress64 address,
  RequestCallback_t callback,
  const size_t timeout)
{
  if (callback)
    this->registerRequest(callback);
  else
    this->unregisterRequest();
  XBJRPCDBG(Serial.print(F("Sending request "));
            serializeJson(request, Serial);
            Serial.print(F(" to address 0x"));
            Serial.print(address.getMsb(), HEX);
            Serial.print(address.getLsb(), HEX););
  ZBTxRequest txRequest;
  txRequest.setAddress64(address);
  char payload[xbeeMaxPayloadSize];
  serializeMsgPack(request, payload);
  txRequest.setPayload(reinterpret_cast<uint8_t*>(payload),
                       measureMsgPack(request));
  unsigned long timeBeforeSend = millis();
  uint8_t error = this->sendAndWait(txRequest, timeout);
  if (not error) {
    XBJRPCDBG(Serial.print(F("Succesfully sent request")););
    const size_t waitTimeout = timeout - (millis() - timeBeforeSend);
    const unsigned long timeBeforeWait = millis();
    while (this->requestCallbackPending()) {
      if (millis() - timeBeforeWait > waitTimeout) {
        XBJRPCDBG(Serial.print(F("Response took too long. Giving up."));)
        this->unregisterRequest();
        return false;
      }
      this->loop();
    }
    return true;
  } else {
    XBJRPCDBG(Serial.print(F("Failed to send packet. Error: 0x"));
              Serial.print(error, HEX););
    this->unregisterRequest();
    return false;
  }
}

template<size_t N_METHODS, size_t INPUT_CAP, size_t OUTPUT_CAP>
void
XBeeJsonRPC<N_METHODS, INPUT_CAP, OUTPUT_CAP>::broadcast(
  const JsonVariantConst request)
{
  XBJRPCDBG(Serial.print(F("Broadcasting request: "));
            serializeJson(request, Serial););
  ZBTxRequest txRequest;
  txRequest.setAddress64(ZB_BROADCAST_ADDRESS);
  char payload[xbeeMaxPayloadSize];
  serializeMsgPack(request, payload);
  txRequest.setPayload(reinterpret_cast<uint8_t*>(payload),
                       measureMsgPack(request));
  this->send(txRequest);
}

template<size_t N_METHODS, size_t INPUT_CAP, size_t OUTPUT_CAP>
bool
XBeeJsonRPC<N_METHODS, INPUT_CAP, OUTPUT_CAP>::sendNotification(
  const JsonVariantConst notification,
  XBeeAddress64 address,
  const size_t timeout)
{
  XBJRPCDBG(Serial.print(F("Sending notification"));
            serializeJson(notification, Serial);
            Serial.print(F(" to address 0x"));
            Serial.print(address.getMsb(), HEX);
            Serial.print(address.getLsb(), HEX););
  ZBTxRequest txRequest;
  txRequest.setAddress64(address);
  char payload[xbeeMaxPayloadSize];
  serializeMsgPack(notification, payload);
  txRequest.setPayload(reinterpret_cast<uint8_t*>(payload),
                       measureMsgPack(notification));
  uint8_t error = this->sendAndWait(txRequest, timeout);
  if (not error) {
    XBJRPCDBG(Serial.print(F("Succesfully send notification.")););
    return true;
  } else {
    XBJRPCDBG(Serial.print(F("Failed to send packet. Error: 0x"));
              Serial.print(error, HEX););
    return false;
  }
}
}
